import APP_STATE from './app-state.js'

const DEFAULT_STATE = {
  "surveyStarted": false,
  "currentAppState": APP_STATE.loading,
  "currentQuestionIndex": 0,
  "answers": [],
  "questions": [],
  "fetchQuestionsSuccess": false
}

export default class Model {
  constructor() {
    this.state = JSON.parse(localStorage.getItem('state')) || DEFAULT_STATE
  }

  get surveyStarted() {
    return this.state.surveyStarted
  }

  get currentQuestionAnswer() {
    const { answers, currentQuestionIndex } = this.state
    return answers[currentQuestionIndex] ? answers[currentQuestionIndex] : null
  }

  get currentQuestion() {
    const { questions, currentQuestionIndex } = this.state
    return questions[currentQuestionIndex] ? questions[currentQuestionIndex] : null
  }

  get currentAppState() {
    return this.state.currentAppState
  }

  get currentQuestionIndex() {
    return this.state.currentQuestionIndex
  }

  get isLastQuestion() {
    return this.state.currentQuestionIndex === this.state.questions.length - 1
  }

  get questionsLength() {
    return this.state.questions.length
  }

  get fetchQuestionsSuccess() {
    return this.state.fetchQuestionsSuccess
  }

  buildPayloadData() {
    let index = 0
    let payload = []
    for (const answer of this.state.answers) {
      payload.push({ question: this.state.questions[index].question, value: answer })
      index++
    }

    return payload
  }

  bindChangeListener(changeListener) {
    this.changeListener = changeListener
  }

  resetState() {
    this.state = DEFAULT_STATE
    this._commitStateChange()
  }

  resetAnswers() {
    this.state.surveyStarted = false
    this.state.currentAppState = APP_STATE.welcome
    this.state.currentQuestionIndex = 0
    this.state.answers = []
    this._commitStateChange()
  }

  startSurvey() {
    this.state.surveyStarted = true
    this._commitStateChange()
  }

  updateAnswer(answer) {
    const { currentQuestionIndex } = this.state
    this.state.answers[currentQuestionIndex] = answer
    this._commitStateChange()
  }

  updateQuestionIndex(index) {
    this.state.currentQuestionIndex = index
    this._commitStateChange()
  }

  upadateCurrentAppState(appState) {
    this.state.currentAppState = appState
    this._commitStateChange()
  }

  updateQuestionsResponse(questions) {
    this.state.questions = questions
    this.state.currentAppState = APP_STATE.welcome
    this.state.fetchQuestionsSuccess = true
    this._commitStateChange()
  }

  resetErrorState() {
    this.state.currentAppState = APP_STATE.welcome
    this.state.currentQuestionIndex = 0
    this._commitStateChange()
  }

  _commitStateChange() {
    localStorage.setItem('state', JSON.stringify(this.state));
    this.changeListener(this.state)
  }


}

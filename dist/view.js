import Question from './components/question.js'
import Buttons from './components/buttons.js'
import Welcome from './components/welcome.js'
import Confirmation from './components/confirmation.js'
import Loader from './components/loader.js'
import ErrorComponent from './components/error.js'

export default class View {
  constructor() {

  }

  getElement(selector) {
    const element = document.querySelector(selector)

    return element
  }

  renderQuestion(type, question, answer, options) {
    const base = this.getElement('#content')
    base.classList.remove("center");
    if (this.question) {
      this.question.update(type, question, answer, options)
    } else {
      this.question = new Question(type, question, answer, options, base, this.onAnswerChange)
    }
  }

  renderButtons(answer, canSubmit) {
    if (this.buttons) {
      this.updateAnswer(answer, canSubmit)
    } else {
      const buttonsBase = this.getElement('#footer-container')
      this.buttons = new Buttons(answer, buttonsBase, this.onNextClick, this.onBackClick)
    }
  }

  renderWelcomeText() {
    const base = this.getElement('#content')
    base.classList.add("center");
    new Welcome(base, this.onStartClick)
  }

  renderConfirmationText() {
    const base = this.getElement('#content')
    base.classList.add("center");
    new Confirmation(base)
  }

  renderLoader() {
    const base = this.getElement('#content')
    base.classList.add("center");
    new Loader(base)
  }

  renderError() {
    const base = this.getElement('#content')
    base.classList.add("center");
    new ErrorComponent(base)
  }

  updateAnswer(answer, canSubmit) {
    this.buttons ? this.buttons.updatedAnswer(answer, canSubmit) : null
  }

  hideButtons() {
    this.buttons ? this.buttons.hide() : null
  }

  bindOnStartClick(onStartClick) {
    this.onStartClick = onStartClick
  }

  bindOnAnswerChange(onChangeCallback) {
    this.onAnswerChange = onChangeCallback
  }

  bindOnNextClick(onNextClick) {
    this.onNextClick = onNextClick
  }

  bindOnBackClick(onBackClick) {
    this.onBackClick = onBackClick
  }
}

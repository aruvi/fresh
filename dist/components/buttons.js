export default class Buttons {
  constructor(answer, container, onNextClick, onBackClick) {
    this.answer = answer
    this.container = container
    this.onNextClick = onNextClick
    this.onBackClick = onBackClick

    this.render()
  }

  updatedAnswer(answer, canSubmit) {
    this.answer = answer
    this.canSubmit = canSubmit
    this.update()
  }

  addEventListeners() {
    this.nextButton.addEventListener('click', (e) => {
      this.onNextClick()
    })

    this.backButton.addEventListener('click', (e) => {
      this.onBackClick()
    })
  }

  static markUp({ answer }) {
    const nextButtonText = 'Next'
    const isDisabled = !answer
    return (
      `
      <button id="back" class="secondary">
        <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24">
          <rect fill="none" height="24" width="24" />
          <path d="M9,19l1.41-1.41L5.83,13H22V11H5.83l4.59-4.59L9,5l-7,7L9,19z" />
        </svg>
         Back
      </button>
      <button ${isDisabled ? 'disabled' : ''} class="primary" id="next">
        ${nextButtonText}
        <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24">
          <rect fill="none" height="24" width="24" />
          <path d="M15,5l-1.41,1.41L18.17,11H2V13h16.17l-4.59,4.59L15,19l7-7L15,5z" />
        </svg>
      </button>
      `
    )
  }

  render() {
    this.container.innerHTML = Buttons.markUp(this)
    this.nextButton = this.container.querySelector('#next')
    this.backButton = this.container.querySelector('#back')

    this.addEventListeners()
  }

  update() {
    const nextButonText = this.canSubmit ? 'Submit' : 'Next'
    const svg = `
      <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24" width="24">
        <rect fill="none" height="24" width="24" />
        <path d="M15,5l-1.41,1.41L18.17,11H2V13h16.17l-4.59,4.59L15,19l7-7L15,5z" />
      </svg>
    `
    this.container.style.display = 'flex';
    this.nextButton.disabled = !this.answer;
    this.nextButton.innerHTML = `
        ${nextButonText}
        ${!this.canSubmit ? svg : ''}
        `
  }

  hide() {
    this.container.style.display = 'none';
  }
}

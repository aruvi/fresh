export default class Welcome {
  constructor(container, onProceedClick) {
    this.container = container
    this.onProceedClick = onProceedClick

    this.render()
  }

  addEventListeners() {
    this.proceedButton.addEventListener('click', () => {
      this.onProceedClick()
    })
  }

  static markUp() {
    return (
      `
      <div class="welcome">
        <span class="greeting-1"> Hi! 👋 </span><br><br>
        <span class="greeting-2"> Help us get some insights into the quality of our products</span><br><br>
        <button class="primary" id="proceed-button">
          <div class="button-container">
            <span class="button-text">Proceed</span>
            <svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24" viewBox="0 0 24 24"
              width="24">
              <rect fill="none" height="24" width="24" />
              <path d="M15,5l-1.41,1.41L18.17,11H2V13h16.17l-4.59,4.59L15,19l7-7L15,5z" />
            </svg>
          </div>
        </button>
      </div>
      `
    )
  }

  render() {
    this.container.innerHTML = Welcome.markUp(this)
    this.proceedButton = this.container.querySelector('#proceed-button')

    this.addEventListeners()
  }
}

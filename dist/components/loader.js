export default class Loader {
  constructor(container) {
    this.container = container

    this.render()
  }

  static markUp() {
    return (
      `
      <div class="center overlay">
        <div class="loader">Loading...</div>
      </div>
      `
    )
  }

  render() {
    this.container.innerHTML = Loader.markUp(this)
  }
}

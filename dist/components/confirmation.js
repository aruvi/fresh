export default class Confirmation {
  constructor(container) {
    this.container = container

    this.render()
  }

  static markUp() {
    return (
      `
      <div class="center fadeInLeftBig">
        <div class="tick-box">
          ✔️
        </div><br>
        <span class="thanks-1"> Thank you! </span><br>
        <span class="thanks-2"> Thanks for helping us improve </span><br><br>
      </div>
      `
    )
  }

  render() {
    this.container.innerHTML = Confirmation.markUp(this)
  }
}

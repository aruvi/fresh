export default class Error {
  constructor(container) {
    this.container = container

    this.render()
  }

  static markUp() {
    return (
      `
      <div class="center fadeInLeftBig">
        <span class="error-face">
          😞
        </span>
        <span class="thanks-1"> Something went wrong, Please try again later </span><br><br>
      </div>
      `
    )
  }

  render() {
    this.container.innerHTML = Error.markUp(this)
  }
}

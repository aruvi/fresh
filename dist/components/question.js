export default class Question {
  constructor(type, question, answer, options, container, onAnswerChange) {
    this.type = type
    this.question = question
    this.answer = answer
    this.options = options
    this.container = container
    this.onAnswerChange = onAnswerChange

    this.render()
  }

  renderQuestion(question) {
    return (
      `
        <div class="question">
          <span> ${question} </span>
        </div>
      `
    )
  }

  renderOptionElements(options, answer, type) {
    let optionElements = ''
    for (const option of options) {
      const { value, text } = option
      const isChecked = typeof value === 'boolean' ? JSON.parse(answer) === value : answer === value
      optionElements += `
        ${type === 'boolean' ? '<div class="radio-block">' : ''}
        <input type="radio" id="${value}" class="radio" name="radio" value="${value}" ${isChecked ? 'checked' : ''}>
        <label for="${value}">${text}</label>
        ${type === 'boolean' ? '</div>' : ''}
      `
    }

    return optionElements
  }

  addEventListeners() {
    if (this.textArea) {
      this.textArea.addEventListener('change', this.answerChangeCallBack.bind(this))
    } else {
      const radioButtons = this.radioGroup.querySelectorAll('input')
      console.log('looped', radioButtons)
      for (var i = 0; i < radioButtons.length; i++) {
        radioButtons[i].addEventListener('change', this.answerChangeCallBack.bind(this));
      }
    }
  }

  answerChangeCallBack(event) {
    this.onAnswerChange(event.target.value)
  }

  static markUp({ type, renderQuestion, renderOptionElements, question, answer, options }) {

    return (
      type === "text" ?
        `
          ${renderQuestion(question)}
          <textarea id="text-area" rows="12" cols="80" name="comment" placeholder="Add you comments here">${answer || ''}</textarea>
        ` :
        `
          ${renderQuestion(question)}
          <div class="select-${type}" id="radio-group">
            ${renderOptionElements(options, answer, type)}
          </div>
        `
    )
  }

  render() {
    this.container.innerHTML = Question.markUp(this)
    this.textArea = this.container.querySelector('#text-area')
    this.radioGroup = this.container.querySelector('#radio-group')

    this.addEventListeners()
  }

  update(type, question, answer, options) {
    this.type = type
    this.question = question
    this.answer = answer
    this.options = options

    this.render()
  }
}

import APP_STATE from './app-state.js'

export default class Controller {
  constructor(model, view) {
    this.model = model
    this.view = view

    this.model.bindChangeListener(this.onStateChange.bind(this))
    this.view.bindOnAnswerChange(this.onAnswerChange.bind(this))
    this.view.bindOnNextClick(this.onNextClick.bind(this))
    this.view.bindOnBackClick(this.onBackClick.bind(this))
    this.view.bindOnStartClick(this.onStartClick.bind(this))

    this.fetchQuestions()
    this.resetErrorState()
    this.renderUI()
  }

  onStateChange(updatedState) {
    this.renderUI()
    console.log('updatedState', updatedState)
  }

  onAnswerChange(answer) {
    this.model.updateAnswer(answer)
    console.log('answer', answer)
  }

  onNextClick() {
    let nextIndex = this.model.currentQuestionIndex + 1

    if (nextIndex > this.model.questionsLength - 1) {
      this.postSurveyData()
    } else {
      this.model.updateQuestionIndex(nextIndex)
    }
  }

  onBackClick() {
    let nextIndex = this.model.currentQuestionIndex - 1

    if (nextIndex < 0) {
      this.model.upadateCurrentAppState(APP_STATE.welcome)
    } else {
      this.model.updateQuestionIndex(nextIndex)
    }
  }

  onStartClick() {
    this.model.startSurvey()
    this.model.upadateCurrentAppState(APP_STATE.question)
    console.log('Start clicked')
  }

  async postSurveyData() {
    try {
      this.model.upadateCurrentAppState(APP_STATE.loading)
      const payload = this.model.buildPayloadData()
      await fetch('api/answers', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(payload),
      })
      this.model.upadateCurrentAppState(APP_STATE.confirmation)
    } catch (e) {
      this.model.upadateCurrentAppState(APP_STATE.error)
    }
  }

  async fetchQuestions() {
    try {
      if (!this.model.fetchQuestionsSuccess) {
        const response = await fetch("api/questions")
        const { questions } = await response.json()
        this.model.updateQuestionsResponse(questions)
      }
    } catch (e) {
      this.model.upadateCurrentAppState(APP_STATE.error)
    }
  }

  resetErrorState() {
    if (this.model.upadateCurrentAppState === APP_STATE.error) {
      this.model.resetErrorState()
    }
  }

  renderQuestion(type, questions, answer, options) {
    this.view.renderQuestion(type, questions, answer, options)
  }

  renderButtons(answer) {
    this.view.renderButtons(answer, this.model.isLastQuestion)
  }

  hideButtons() {
    this.view.hideButtons()
  }

  renderWelcomeText() {
    this.view.renderWelcomeText()
  }

  renderConfirmationText() {
    this.view.renderConfirmationText()
    setTimeout(this.model.resetAnswers.bind(this.model), 5000)
  }

  renderLoader() {
    this.view.renderLoader()
  }

  renderError() {
    this.view.renderError()
  }

  renderUI() {
    switch (this.model.currentAppState) {
      case APP_STATE.loading:
        this.renderLoader()
        break
      case APP_STATE.error:
        this.renderError()
        this.hideButtons()
        break
      case APP_STATE.welcome:
        this.renderWelcomeText()
        this.hideButtons()
        break
      case APP_STATE.confirmation:
        this.renderConfirmationText()
        this.hideButtons()
        break
      default:
        const currentAnswer = this.model.currentQuestionAnswer
        const { type, question, options } = this.model.currentQuestion
        this.renderQuestion(type, question, currentAnswer, options)
        this.renderButtons(this.model.currentQuestionAnswer)
    }
  }


}

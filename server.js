const questions = require('./questions')
const express = require('express')
const cors = require('cors')
const path = require('path')
const bodyParser = require('body-parser');
const minify = require('express-minify');
const compression = require('compression')
const port = process.env.PORT || 3000
const app = express()
const router = express.Router();

app.use(cors())

app.use(compression());
app.use(minify());

app.use(bodyParser.json());

app.use(express.static(__dirname + '/dist'))

router.get('/questions', function (req, res) {
  res.json(questions);
});

router.post('/answers', function (req, res) {
  console.log(req.body)
  res.json({ message: "success" });
});

app.use('/api', router);


app.get('*', function (request, response) {
  response.sendFile(path.resolve(__dirname, 'dist', 'index.html'))
})

app.listen(port)
console.log("server started on port " + port)
